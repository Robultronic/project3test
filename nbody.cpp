#include "nbody.h"
#include "armadillo"
#include <iostream>
#include <fstream>

double G = 4.0*M_PI*M_PI;

using namespace arma;
using namespace std;

nbody::nbody(vec marg, vec riniarg, vec viniarg, double tendarg, double tiniarg, int Narg, int narg)
{
    rini = riniarg; //Initial positions.
    vini = viniarg; //Initial velocities.
    tini = tiniarg; //Initial time.
    tend = tendarg; //End time.
    m = marg; //Masses of the individual bodies.
    N = Narg; //Number of steps.
    n = narg; //Number of bodies.
    cout << N << "  " << n << endl;
    r = zeros<mat>(2*n,N);
    v = zeros<mat>(2*n,N);
}

//Contribution to the acceleration in x-direction from one body:
double nbody::ax(double t,double x, double y, double m)
{
    //cout << "factors: " << x << "  " << y << "  " << m << endl;
    double x2 = pow(x, 2.0);
    double y2 = pow(y, 2.0);
    double a_x = (G*m/pow(x2+y2,3.0/4.0))*x;
    //cout << "a_x: " << a_x << endl;
    return a_x;
}

//Contribution to the acceleration in y-direction from one body:
double nbody::ay(double t,double x, double y, double m)
{
    double x2 = pow(x, 2.0);
    double y2 = pow(y, 2.0);
    double a_y = (G*m/pow(x2+y2,3.0/4.0))*y;
    //cout << "a_y: " << a_y << endl;
    return a_y;
}

//Velocity in x-direction:
double nbody::vx(double t, double vx, double vy, double m)
{
    return vx;
}

//Velocity in y-direction:
double nbody::vy(double t, double vx, double vy, double m)
{
    return vy;
}

//Rungekutta of the 4th order for n bodies:
void nbody::RK4(int except, int centermass)
{
    //cout << N << "  " << n << endl;
    //cout << "1" << endl;
    double xdist,ydist;
    double ktot,ltot,qxtot,qytot,k1,k2,k3,k4,l1,l2,l3,l4,qx1,qx2,qx3,qx4,qy1,qy2,qy3,qy4;
    double h = (tend - tini)/double(N-1.0);

    for (int b = 0;b<2*n;b++)
    {
        r(b,0) = rini(b);
        v(b,0) = vini(b);
        //cout << b << endl;
    }

    int inumber = 0;
    //Set origo to the center of mass if told to do so:
    if (centermass == 1)
    {
        double mtot = 0.0;
        double cmassfacx = 0.0;
        double cmassfacy = 0.0;
        for (int j = 0;j<n;j++)
        {
            mtot += m(j);
            cmassfacx += m(j)*r(2*j,inumber);
            cmassfacy += m(j)*r(2*j+1,inumber);
        }
        vec cmass = zeros(2);
        cmass(0) = cmassfacx/mtot;
        cmass(1) = cmassfacy/mtot;
        for (int j = 0;j<n;j++)
        {
            r(2*j,inumber) -= cmass(0);
            r(2*j+1,inumber) -= cmass(1);
        }

    }
    else {}

    //cout << r.col(0) << endl;
    //Loop that advances time:
    for (int i = 0;i<N-1;i++)
    {
        //cout << i << endl;
        double t = tini + i*h;
        //Loops over all bodies:
        for (int j = 0;j<n;j++)
        {
            if (j==except) continue;
            else
            {
                //cout << j << endl;
                k1 = 0.0;
                k2 = 0.0;
                k3 = 0.0;
                k4 = 0.0;
                l1 = 0.0;
                l2 = 0.0;
                l3 = 0.0;
                l4 = 0.0;
                qx1 = 0.0;
                qx2 = 0.0;
                qx3 = 0.0;
                qx4 = 0.0;
                qy1 = 0.0;
                qy2 = 0.0;
                qy3 = 0.0;
                qy4 = 0.0;
                //cout << "k1: " << k1 << endl;
                //Loop over forces from all the other bodies:
                for (int k = 0;k<n;k++)
                {
                    //cout << k << "   " << j << endl;
                    if (k == j) {}
                    else { //loop over bodies other than j we get the total acceleration.
                        xdist = r(2*k,i) - r(2*j,i);
                        ydist = r(2*k+1,i) - r(2*j+1,i);
                        //cout << k << "  " << xdist << "  " << ydist << endl;
                        k1 += ax(t,xdist,ydist,m(k));
                        l1 += ay(t,xdist,ydist,m(k));
                    }

                    //cout << "here" << endl;
                    //else {continue;}
                }
                qx1 = vx(t,v(2*j,i),v(2*j+1,i),m(j));
                qy1 = vy(t,v(2*j,i),v(2*j+1,i),m(j));
                //cout << "1" << endl;
                for (int k = 0;k<n;k++)
                {
                    if (k == j) continue;
                    else {
                        xdist = r(2*k,i) - r(2*j,i);
                        ydist = r(2*k+1,i) - r(2*j+1,i);
                        k2 += ax(t+0.5*h,xdist+0.5*h*k1,ydist+0.5*h*l1,m(k));
                        l2 += ay(t+0.5*h,xdist+0.5*h*k1,ydist+0.5*h*l1,m(k));
                    }

                }
                qx2 = vx(t+0.5*h,v(2*j,i)+0.5*h*qx1,v(2*j+1,i)+0.5*h*qy1,m(j));
                qy2 = vy(t+0.5*h,v(2*j,i)+0.5*h*qx1,v(2*j+1,i)+0.5*h*qy1,m(j));
                for (int k = 0;k<n;k++)
                {
                    if (k == j) continue;
                    else {
                        xdist = r(2*k,i) - r(2*j,i);
                        ydist = r(2*k+1,i) - r(2*j+1,i);
                        k3 += ax(t+0.5*h,xdist+0.5*h*k2,ydist+0.5*h*l2,m(k));
                        l3 += ay(t+0.5*h,xdist+0.5*h*k2,ydist+0.5*h*l2,m(k));
                    }

                }
                qx3 = vx(t+0.5*h,v(2*j,i)+0.5*h*qx2,v(2*j+1,i)+0.5*h*qy2,m(j));
                qy3 = vy(t+0.5*h,v(2*j,i)+0.5*h*qx2,v(2*j+1,i)+0.5*h*qy2,m(j));
                for (int k = 0;k<n;k++)
                {
                    if (k == j) continue;
                    else {
                        xdist = r(2*k,i) - r(2*j,i);
                        ydist = r(2*k+1,i) - r(2*j+1,i);
                        k4 += ax(t+h,xdist+h*k3,ydist+h*l3,m(k));
                        l4 += ay(t+h,xdist+h*k3,ydist+h*l3,m(k));
                    }

                }
                qx4 = vx(t+h,v(2*j,i)+h*qx3,v(2*j+1,i)+h*qy3,m(j));
                qy4 = vy(t+h,v(2*j,i)+h*qx3,v(2*j+1,i)+h*qy3,m(j));

                ktot = (1.0/6.0)*(k1+2.0*k2+2.0*k3+k4);
                ltot = (1.0/6.0)*(l1+2.0*l2+2.0*l3+l4);
                v(2*j,i+1) = v(2*j,i) + h*ktot;
                v(2*j+1,i+1) = v(2*j+1,i) + h*ltot;
                qxtot = (1.0/6.0)*(qx1+2.0*qx2+2.0*qx3+qx4);
                qytot = (1.0/6.0)*(qy1+2.0*qy2+2.0*qy3+qy4);
                r(2*j,i+1) = r(2*j,i) + h*qxtot;
                r(2*j+1,i+1) = r(2*j+1,i) + h*qytot;

                //Shift origo to the senset of mass if told to do so:
                if (centermass == 1)
                {
                    double mtot = 0.0;
                    double cmassfacx = 0.0;
                    double cmassfacy = 0.0;
                    for (int j = 0;j<n;j++)
                    {
                        mtot += m(j);
                        cmassfacx += m(j)*r(2*j,i+1);
                        cmassfacy += m(j)*r(2*j+1,i+1);
                    }
                    vec cmass = zeros(2);
                    cmass(0) = cmassfacx/mtot;
                    cmass(1) = cmassfacy/mtot;
                    for (int j = 0;j<n;j++)
                    {
                        r(2*j,i+1) -= cmass(0);
                        r(2*j+1,i+1) -= cmass(1);
                    }

                }
                else continue;
            }
        }
    }
    //return r,v;
}

//Euler to check my RK4 against:
void nbody::Euler(int except)
{
    //cout << N << "  " << n << endl;
    //cout << "1" << endl;
    double xdist,ydist;
    double ktot,ltot,k1,k2,k3,k4,l1,l2,l3,l4;
    double h = (tend - tini)/double(N-1.0);
    cout << "h: " << h << endl;

    for (int b = 0;b<2*n;b++)
    {
        r(b,0) = rini(b);
        v(b,0) = vini(b);
        //cout << b << endl;
    }

    //cout << r.col(0) << endl;
    for (int i = 0;i<N-1;i++)
    {
        //cout << i << endl;
        double t = tini + i*h;
        for (int j = 0;j<n;j++)
        {
            //cout << "j: " << j << endl;
            k1,k2,k3,k4,l1,l2,l3,l4 = 0.0;
            k1 = 0.0;
            l1 = 0.0;
            //cout << "pre k1: " << k1 << endl;
            for (int k = 0;k<n;k++)
            {
                //cout << k << "   " << j << endl;
                if (k == j) {}
                else {
                    xdist = r(2*k,i) - r(2*j,i);
                    ydist = r(2*k+1,i) - r(2*j+1,i);
                    //cout << k << "  " << xdist << "  " << ydist << endl;
                    k1 += ax(t,xdist,ydist,m(k));
                    l1 += ay(t,xdist,ydist,m(k));
                    //cout << "post k1: " << k1 << endl;
                }

                //cout << "here" << endl;
                //else {continue;}
            }
            //cout << "1" << endl;

            if (j == except) continue;
            else {
                //cout << "a_x: " << k1 << endl;
                v(2*j,i+1) = v(2*j,i) + h*k1;
                v(2*j+1,i+1) = v(2*j+1,i) + h*l1;
                r(2*j,i+1) = r(2*j,i) + h*v(2*j,i+1);
                r(2*j+1,i+1) = r(2*j+1,i) + h*v(2*j+1,i+1);
                //cout << "vxnew: " << v(2*j,i+1) << endl;
                //cout << "rxnew: " << r(2*j,i+1) << endl;
            }

        }
    }
    //return r,v;
}

//Prints the coordinates and velocities of a planet to files:
void nbody::print(int i)
{
    if (i == 0) {
        ofstream rxmyfile;
        rxmyfile.open ("rx0.txt"); //Apparendly the open function cannot
        rxmyfile << r.row(2*i);    //take variables of the string type as argument.
        rxmyfile.close();          //I wanted to pass the filenames to the function
                                   //and avoid all this mess.
        ofstream rymyfile;
        rymyfile.open ("ry0.txt");
        rymyfile << r.row(2*i+1);
        rymyfile.close();

        ofstream vxmyfile;
        vxmyfile.open ("vx0.txt");
        vxmyfile << v.row(2*i);
        vxmyfile.close();

        ofstream vymyfile;
        vymyfile.open ("vy0.txt");
        vymyfile << v.row(2*i+1);
        vymyfile.close();
    }

    if (i == 1) {
        ofstream rxmyfile;
        rxmyfile.open ("rx1.txt");
        rxmyfile << r.row(2*i);
        rxmyfile.close();

        ofstream rymyfile;
        rymyfile.open ("ry1.txt");
        rymyfile << r.row(2*i+1);
        rymyfile.close();

        ofstream vxmyfile;
        vxmyfile.open ("vx1.txt");
        vxmyfile << v.row(2*i);
        vxmyfile.close();

        ofstream vymyfile;
        vymyfile.open ("vy1.txt");
        vymyfile << v.row(2*i+1);
        vymyfile.close();
    }
    if (i == 2) {
        ofstream rxmyfile;
        rxmyfile.open ("rx2.txt");
        rxmyfile << r.row(2*i);
        rxmyfile.close();

        ofstream rymyfile;
        rymyfile.open ("ry2.txt");
        rymyfile << r.row(2*i+1);
        rymyfile.close();

        ofstream vxmyfile;
        vxmyfile.open ("vx2.txt");
        vxmyfile << v.row(2*i);
        vxmyfile.close();

        ofstream vymyfile;
        vymyfile.open ("vy2.txt");
        vymyfile << v.row(2*i+1);
        vymyfile.close();
    }
    if (i == 3) {
        ofstream rxmyfile;
        rxmyfile.open ("rx3.txt");
        rxmyfile << r.row(2*i);
        rxmyfile.close();

        ofstream rymyfile;
        rymyfile.open ("ry3.txt");
        rymyfile << r.row(2*i+1);
        rymyfile.close();

        ofstream vxmyfile;
        vxmyfile.open ("vx3.txt");
        vxmyfile << v.row(2*i);
        vxmyfile.close();

        ofstream vymyfile;
        vymyfile.open ("vy3.txt");
        vymyfile << v.row(2*i+1);
        vymyfile.close();

    }
    if (i == 4) {
        ofstream rxmyfile;
        rxmyfile.open ("rx4.txt");
        rxmyfile << r.row(2*i);
        rxmyfile.close();

        ofstream rymyfile;
        rymyfile.open ("ry4.txt");
        rymyfile << r.row(2*i+1);
        rymyfile.close();

        ofstream vxmyfile;
        vxmyfile.open ("vx4.txt");
        vxmyfile << v.row(2*i);
        vxmyfile.close();

        ofstream vymyfile;
        vymyfile.open ("vy4.txt");
        vymyfile << v.row(2*i+1);
        vymyfile.close();

    }
    if (i == 5) {
        ofstream rxmyfile;
        rxmyfile.open ("rx5.txt");
        rxmyfile << r.row(2*i);
        rxmyfile.close();

        ofstream rymyfile;
        rymyfile.open ("ry5.txt");
        rymyfile << r.row(2*i+1);
        rymyfile.close();

        ofstream vxmyfile;
        vxmyfile.open ("vx5.txt");
        vxmyfile << v.row(2*i);
        vxmyfile.close();

        ofstream vymyfile;
        vymyfile.open ("vy5.txt");
        vymyfile << v.row(2*i+1);
        vymyfile.close();

    }
    if (i == 6) {
        ofstream rxmyfile;
        rxmyfile.open ("rx6.txt");
        rxmyfile << r.row(2*i);
        rxmyfile.close();

        ofstream rymyfile;
        rymyfile.open ("ry6.txt");
        rymyfile << r.row(2*i+1);
        rymyfile.close();

        ofstream vxmyfile;
        vxmyfile.open ("vx6.txt");
        vxmyfile << v.row(2*i);
        vxmyfile.close();

        ofstream vymyfile;
        vymyfile.open ("vy6.txt");
        vymyfile << v.row(2*i+1);
        vymyfile.close();

    }
    if (i == 7) {
        ofstream rxmyfile;
        rxmyfile.open ("rx7.txt");
        rxmyfile << r.row(2*i);
        rxmyfile.close();

        ofstream rymyfile;
        rymyfile.open ("ry7.txt");
        rymyfile << r.row(2*i+1);
        rymyfile.close();

        ofstream vxmyfile;
        vxmyfile.open ("vx7.txt");
        vxmyfile << v.row(2*i);
        vxmyfile.close();

        ofstream vymyfile;
        vymyfile.open ("vy7.txt");
        vymyfile << v.row(2*i+1);
        vymyfile.close();

    }
    if (i == 8) {
        ofstream rxmyfile;
        rxmyfile.open ("rx8.txt");
        rxmyfile << r.row(2*i);
        rxmyfile.close();

        ofstream rymyfile;
        rymyfile.open ("ry8.txt");
        rymyfile << r.row(2*i+1);
        rymyfile.close();

        ofstream vxmyfile;
        vxmyfile.open ("vx8.txt");
        vxmyfile << v.row(2*i);
        vxmyfile.close();

        ofstream vymyfile;
        vymyfile.open ("vy8.txt");
        vymyfile << v.row(2*i+1);
        vymyfile.close();

    }
    if (i == 9) {
        ofstream rxmyfile;
        rxmyfile.open ("rx9.txt");
        rxmyfile << r.row(2*i);
        rxmyfile.close();

        ofstream rymyfile;
        rymyfile.open ("ry9.txt");
        rymyfile << r.row(2*i+1);
        rymyfile.close();

        ofstream vxmyfile;
        vxmyfile.open ("vx9.txt");
        vxmyfile << v.row(2*i);
        vxmyfile.close();

        ofstream vymyfile;
        vymyfile.open ("vy9.txt");
        vymyfile << v.row(2*i+1);
        vymyfile.close();

    }
    if (i == 10) {
        ofstream rxmyfile;
        rxmyfile.open ("rx10.txt");
        rxmyfile << r.row(2*i);
        rxmyfile.close();

        ofstream rymyfile;
        rymyfile.open ("ry10.txt");
        rymyfile << r.row(2*i+1);
        rymyfile.close();

        ofstream vxmyfile;
        vxmyfile.open ("vx10.txt");
        vxmyfile << v.row(2*i);
        vxmyfile.close();

        ofstream vymyfile;
        vymyfile.open ("vy10.txt");
        vymyfile << v.row(2*i+1);
        vymyfile.close();

    }
}

