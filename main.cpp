#include <iostream>
#include "armadillo"
#include "nbody.h"
#include <cmath>
using namespace std;
using namespace arma;

double ax(double, double, double);
double ay(double, double, double);

int main()
{
    cout << pow(7.0+3.0,2.0) << endl;
    double m_sunkg = 2.0*pow(10.0,30.0);
    double m_sun = 1.0;
    double m_mercury = 2.4*pow(10.0,23.0)/m_sunkg;
    double m_venus = 4.9*pow(10.0,24.0)/m_sunkg;
    double m_earth = 6.0*pow(10.0,24.0)/m_sunkg;
    double m_mars = 6.6*pow(10.0,23.0)/m_sunkg;
    double m_jupiter = 1.9*pow(10.0,27.0)/m_sunkg;
    double m_saturn = 5.5*pow(10.0,26.0)/m_sunkg;
    double m_uranus = 8.8*pow(10.0,25.0)/m_sunkg;
    double m_neptune = 1.03*pow(10.0,26.0)/m_sunkg;
    double m_pluto = 1.31*pow(10.0,22.0)/m_sunkg;
    cout << m_earth << endl;
    int n = 2;
    int N = 5000;
    double tini = 0.0;
    double tend = 1.0;
    vec mvec = zeros(n);
    mvec(0) = m_sun, mvec(1) = m_earth;// mvec(2) = m_jupiter;
    vec rini = zeros(2*n);
    rini(0) = 0.0, rini(1) = 0.0, rini(2) = -1.0, rini(3) = -0.0; //rini(4) = 1.0,
            //rini(5) = 0.0;
    vec vini = zeros(2*n);
    vini(0) = 0.0, vini(1) = 0.0, vini(2) = 0.0*M_PI, vini(3) = 2.0*M_PI;
    cout << "vy1ini: " << vini(3) << endl;
            //vini(4) = 0.0, vini(5) = 2.0*M_PI*5.20;

    /*nbody earthjup(mvec, rini, vini,tend, tini, N, n);
    earthjup.RK4(100, 1);
    earthjup.print(0);
    earthjup.print(1);
    earthjup.print(2);*/
    nbody taska(mvec, rini, vini, tend, tini, N, n);
    taska.RK4(0, 0);
    //taska.Euler(0);
    taska.print(1);
    return 0;
}
