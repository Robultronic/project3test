from scitools.std import *
import numpy as np

#Reading vector:

def vecread(filename):
	table = open(filename, 'r')
	rows = table.readlines()
	n = 1
	m = len(rows)
	#print m
	c = 0

	for i in rows:
		row = i.split(' ')
		#row = ''.join(row)
		row = filter(lambda number: number.strip(), row)
		#print row
		elements = np.zeros(len(row))
		for k in range(len(row)):
			elements[k] = float(row[k])
		c += 1
	return elements
#rx0 = vecread("rx0.txt")
#ry0 = vecread("ry0.txt")
rx1 = vecread("rx1.txt")
ry1 = vecread("ry1.txt")
#rx2 = vecread("rx2.txt")
#ry2 = vecread("ry2.txt")
t = linspace(0.0,0.1,len(ry1))
#print rx
'''print len(v), len(vluf)
x = linspace(0,1,len(v))
u = 1.0 - (1.0 - exp(-10.0))*x - exp(-10.0*x)
'''
#Making and saving plots:
figure(1)
plot(rx1,ry1, xlabel = "x", ylabel = "y", legend = "Earth", hardcopy = "KE2.ps")
'''
hold('on')
plot(rx2,ry2, legend = "Jupiter")
plot(rx0,ry0, legend = "Sun")

hold('on')

plot(x,u, legend = "Analytical")

figure(2)
plot(x,vluf, title = "LU decomp vs analytical solution for n = %.f" % (len(vluf) - 2), xlabel = "x", ylabel = "u(x)", legend = "LU", hardcopy = "lastlun%.f.png" % (len(vluf) - 2))

hold('on')

plot(x,u, legend = "Analytical")
'''
'''
figure(3)
plot(x,u, title = "Only analytical")'''
