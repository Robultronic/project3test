#ifndef PLANET_H
#define PLANET_H
#include "armadillo"
#include <iostream>
#include <fstream>

using namespace std;
using namespace arma;

class nbody
{
public:
    vec rini;
    vec vini;
    double tini;
    double tend;
    int n;
    int N;
    vec m;
    mat r;
    mat v;

    nbody(vec marg, vec riniarg, vec viniarg, double tendarg, double tiniarg, int Narg, int narg);
    double ax(double t,double x, double y, double m);
    double ay(double t,double x, double y, double m);
    double vx(double t,double vx, double vy, double m);
    double vy(double t,double vx, double vy, double m);
    void RK4(int except, int centermass);
    void Euler(int except);
    void print(int i);
};

#endif // PLANET_H
